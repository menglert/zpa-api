import logging
import json
import requests
from authenticateSession import headers
from authenticateSession import zpaCloud

logging.basicConfig(level=logging.DEBUG, filename='zpa-api.log')

def endSession():
    res = requests.post("https://config." + zpaCloud + "/signout", headers=headers)
    logging.debug("Getting Response for endSession: " + res.text)

endSession()