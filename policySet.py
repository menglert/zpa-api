import logging
import json
import requests
from authenticateSession import headers
from authenticateSession import zpaCloud
from authenticateSession import customerId

logging.basicConfig(level=logging.DEBUG, filename='zpa-api.log')

def getGlobalPolicySet(headers):
    res = requests.get("https://config." + zpaCloud + "/mgmtconfig/v1/admin/customers/" + customerId + "/policySet/global", headers=headers)
    logging.debug("Getting Response for getGlobalPolicySet: " + res.text)

    policySetId = json.loads(res.text)["id"]
    logging.debug("Global policySetId is: " + policySetId)

    return policySetId

def postPolicy(policySetId, payload):
    res = requests.post("https://config." + zpaCloud + "/mgmtconfig/v1/admin/customers/" + customerId + "/policySet/" + policySetId + "/rule", headers=headers, data=json.dumps(payload))
    logging.debug("Getting Response for postPolicy: " + res.text)
