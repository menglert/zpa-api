from authenticateSession import headers
from policySet import postPolicy
from policySet import getGlobalPolicySet

policyPayload = {
    "conditions": [],
    "name": "PolicyName",
    "description": "Description",
    "action": "ALLOW",
    "customMsg": "MsgString"
}

policySetId = getGlobalPolicySet(headers)
postPolicy(policySetId, policyPayload)