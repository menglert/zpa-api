import requests
import json
import getpass
import keyring
import logging
from requests.structures import CaseInsensitiveDict

logging.basicConfig(level=logging.DEBUG, filename='zpa-api.log')

try:
    configFile = open("config.json")
    logging.info("Reading configuration from: " + configFile.name)
    
    configJson = json.load(configFile)
    apiKeyName = configJson["apiKeyName"]
    zpaCloud = configJson["zpaCloud"]
    customerId = configJson["customerId"]
    clientId = keyring.get_password("zpa-api-clientId", apiKeyName)
    clientSecret = keyring.get_password("zpa-api-clientSecret", apiKeyName)
    if clientId is None or clientSecret is None:
        raise IOError("No Credential set.")
    logging.info("Read Client ID for API Key Name : " + apiKeyName + " for Cloud: " + zpaCloud + " and Client Secret")
except IOError:
    logging.info("No config present. Creating one.")
    configJson = {}
    apiKeyName = input("API Key Name: ")
    customerId = input("Customer ID: ")
    clientId = getpass.getpass(prompt="Client ID: ")
    clientSecret = getpass.getpass(prompt="Client Secret: ")
    zpaCloud = input("ZPA Cloud: ")
    configJson["apiKeyName"] = apiKeyName
    configJson["zpaCloud"] = zpaCloud
    configJson["customerId"] = customerId
    configFile = open("config.json", "w")
    json.dump(configJson, configFile)
    logging.info("Wrote API Key Name: " + apiKeyName + "and Customer ID: " + customerId + " for Cloud: " + zpaCloud + " in " + configFile.name)

    configFile.close()
    keyring.set_password("zpa-api-clientId", apiKeyName, clientId)
    keyring.set_password("zpa-api-clientSecret", apiKeyName, clientSecret)
    logging.info("Set API Key Name: " + apiKeyName + " for Cloud: " + zpaCloud + " and Client Secret in local Key Store")
finally:
    configFile.close()

payload = {"client_id": clientId, "client_secret": clientSecret}

headers = {
    'content-type': "application/x-www-form-urlencoded",
}
logging.debug("Using Headers: " + str(headers))

res = requests.post("https://config." + zpaCloud + "/signin", headers=headers, data=payload)
logging.debug("Auth Response: " + res.text)

headers = CaseInsensitiveDict()
headers["Authorization"] = "Bearer " + json.loads(res.text)["access_token"]
headers["Content-Type"] = "application/json"
logging.debug("Using Headers: " + str(headers))
logging.info("Logged In.")